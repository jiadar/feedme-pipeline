# Scorecard

This utility calculates the Health Score 2.0 on the menu_data table, producing a data.txt which can be
imported into postgres (psql < data.txt)

To run:
rm data.txt; mix escript.build; time ./scorecard

