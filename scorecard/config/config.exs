use Mix.Config

config :scorecard, ecto_repos: [Scorecard.Repo]

if Mix.env == :dev do
  config :mix_test_watch,
    setup_tasks: ["ecto.reset"],
    ansi_enabled: :ignore,
    clear: true
end

config :logger, :console,
  level: :error

import_config "#{Mix.env}.exs"
