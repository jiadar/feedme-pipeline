<h2>USDA Processor</h2>

tl;dr

TO GENERATE A FILE AND IMPORT INTO POSTGRES:
1. Apply the schema migrations sql/migrations-*.sql
2. Compile (mvn clean install)
3. Unzip the USDA_FULL files in /PATH/TO/USDA_DIR
4. Run the JAR: java -jar target/UsdaProcessor-1.0-SNAPSHOT.jar /PATH/TO/USDA_DIR
5. Load the generate.out into postgres: psql --username YOUR_USERNAME --password DATABASE < generated.out

More Details: 

Docs are in doc/index.html
To build: mvn clean install
To run: java -jar target/UsdaProcessor-1.0-SNAPSHOT.jar /USDA_DIR
To create javadoc: mvn javadoc:javadoc

The bin directory has various utilities. You may have to edit the scripts for your local directory,
and add the bin directory to your path: 

compile - Cleans and compiles the project
db - enters the postgres db
dep - resolves and displays dependencies
doc - generates javadoc
run - runs the jar
t - runs the tests
up - updates and runs , can give a number to run only a subset

Workflow
I change the program then run up 10 to make sure it works.
Once it works I run up 10000
If I am testing or writing a new importer I will try to import 10000
Then I run up to update everyhing (up with no params)

=======
<b>tl;dr</b>

TO GENERATE A FILE AND IMPORT INTO POSTGRES:
<ol>
<li> Apply the schema migrations sql/migrations-*.sql
<li> Compile (mvn clean install)
<li> Unzip the USDA_FULL files in /PATH/TO/USDA_DIR
<li> Run the JAR: java -jar target/UsdaProcessor-1.0-SNAPSHOT.jar /PATH/TO/USDA_DIR
<li> Load the generate.out into postgres: psql --username YOUR_USERNAME --password DATABASE < generated.out
</ul>

<b>More Details: <b>

Docs are in doc/index.html
To build: mvn clean install
To run: java -jar target/UsdaProcessor-1.0-SNAPSHOT.jar /USDA_DIR
To create javadoc: mvn javadoc:javadoc

<b>bin utilities</b>

The bin directory has various utilities. You may have to edit the scripts for your local directory,
and add the bin directory to your path and set PGPASSWORD environment variable.  
<ul>
<li>compile - Cleans and compiles the project
<li>db - enters the postgres db
<li>dbload - loads the postgres db by dropping the table, running the migration, and loading generated.out
<li>dep - resolves and displays dependencies
<li>doc - generates javadoc
<li>run - runs the jar
<li>t - runs the tests
<li>up - updates and runs , can give a number to run only a subset
<li>upt - updates and runs, but only for the subset of data in the resources directory
</ul>

<b> Dev / Test Workflow </b>
<ul>
<li>I change the program then run up 10 to make sure it works.
<li>Once it works I run up 10000
<li>If I am testing or writing a new importer I will try to import 10000
<li>Then I run up to update everyhing (up with no params)
<li>If I am trying to track down a bug, and the bug exists on the subset of files in the test/resources directory
then I run upt and dbload to test with that subset of 8 files.
</ul>


