package co.fabrichealth.utils;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;
import co.fabrichealth.utils.Nutrient;
import co.fabrichealth.utils.parsers.UsdaParser;
import co.fabrichealth.utils.parsers.BrandedParser;
import co.fabrichealth.utils.parsers.StdParser;

/** 
 * Represents a single ingredient or packaged food product, which is in a single file in the
 * USDA_FULL directory.
 *
 * The main interaction with this class is through the constructors, which will create a new 
 * ingredent in a variety of ways from a file or list of strings in a particular format as 
 * in the USDA_FULL directory. There is also a parse method for this purpose. 
 * 
 */
public class Ingredient {

	// TODO: not the right place for this DataFormat, move to parsers 
	// Enumerate the data formats that we have parsers for
	public enum DataFormat {BRANDED, STD, UNKNOWN};

	/* Instance Vars */
	private String source;
	private Date collection_date;
	private String ndb;
	private String upc;
	private String name;
	private String group;
	private String common_name;
	private List<Nutrient> nutrients;
	private String ingredients;
	private String fn;                       // filename 

	/* Constructors */
	/** Creates an empty ingredient */
	public Ingredient() {
		this.empty();
	}

	/** 
	 * Creates an ingredient from a file 
	 *
	 * @param f File with ingredient data in a Usda format
	 * @throws IOException If the file can not be opened
	 */
	public Ingredient(File f) throws IOException {
		this();
		ArrayList<String> data = new ArrayList<String>();
		String line;
		
		try {
			BufferedReader b = new BufferedReader(new FileReader(f));			
			while ((line = b.readLine()) != null) {
				data.add(line);
			}
			b.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		fn = f.getName(); 
		parse(data);
	}
	
	/** 
	 * Creates an ingredient by parsing a list of strings containing scraped data.  
	 *
	 * @param data A list containing lines of a Usda format file
	 */
	public Ingredient(List<String> data) {
		this();
		parse(data);
	}

	/* Public Methods */

	/** Empty the ingredient by setting all the instance variables to sane defaults */
	public void empty() {
		source = "unknown";
		collection_date = new Date();
		ndb = "";
		upc = "";
		name = "";
		group = "";
		common_name = "";
		nutrients = new ArrayList<Nutrient>();
		ingredients = "";
		fn = "";
	}
	
	/** Parses an ingredient from a list of strings containing scraped data 
	 *
	 * @param data A list of strings to parse
	 */
	public void parse(final List<String> data) {
		parse(data, detect(data));
	}

	/** Parses an ingredient depending on the detected data format. 
	 *
	 * Detect the data format with the helper method and execute the parser for each
	 * recognized type. If the type is not recognized, then set the instance to empty. 
	 *
	 * @param data A list of strings containing the data
	 * @param dataFormat A specified (or detected) data format as defined in the DataFormat Enum
	 */
	private void parse(final List<String> data, DataFormat dataFormat) {
		UsdaParser p = null;
		switch (dataFormat) {
		case BRANDED:
			p = new BrandedParser(data);
			break;
		case STD:
			p = new StdParser(data);
			break;
		case UNKNOWN:
			break;
		}
		if (p != null) {
			source = p.source(); 
			collection_date = p.collection_date();
			ndb = p.ndb();
			upc = p.upc();
			name = p.name();
			group = p.group();
			common_name = p.cn();
			nutrients = p.nutrients();
			ingredients = p.ingredients();
		}
		else {
			this.empty();
		}
	}

	/** 
	 * Detect the format of the data and determines if it is parsable or not 
	 * 
	 * Detection is done based on the first line of the file. If it matches a string then
	 * the format is set and returned. If we don't have a match we return unknown. 
	 *
	 * @param data  List of strings representing a USDA_FULL data file 
	 * @return  The data format type detected, or a data format of UNKNOWN otherwise
	 */
	public DataFormat detect(final List<String> data) {
		if (data.get(0).contains("Branded Food Products Database")) {
			return DataFormat.BRANDED;
		}
		if (data.get(0).contains("Source: USDA National Nutrient Database for Standard Reference 28 slightly revised May 2016 Software v.3.7.1 2017-03-29")) {
			return DataFormat.STD;
		}
		return DataFormat.UNKNOWN;
	}
	
	/**
	 * Finds the first occurance of a nutrient matching the passed regex string. 
	 *
	 * If you want to find a specific nutrient, then include the entire string, otherwise
	 * you may not get the nutrient expected. 
	 *
	 * @param  pattern A regex to search for containing the text of the nutrient desired
	 * @return  Optional.Empty() if the nutrient was not found, 
	 *          Optional.of(nutrient) if the nutrient was found
	 */
	public Optional<Nutrient> findNutrient(final String pattern) {
		Iterator<Nutrient> nutrientIterator = nutrients.iterator();
		Pattern p = Pattern.compile(pattern); 
		while(nutrientIterator.hasNext()) {
			Nutrient n = nutrientIterator.next();
			Matcher m = p.matcher(n.name());
			if (m.find()) {
				return Optional.of(n);
			}
		}
		return Optional.empty();
	}

	/**
	 * Finds a list of nutrients matching the passed regex string. 
	 *
	 * @param  pattern A regex to search for containing the text of the nutrient desired
	 * @return An arraylist containing all the nutrients found. If no nutrient was found, 
	 *         the list will be empty. 
	 */
	public ArrayList<Nutrient> findNutrients(final String pattern) {
		ArrayList<Nutrient> rval = new ArrayList<Nutrient>();
		Iterator<Nutrient> nutrientIterator = nutrients.iterator();
		Pattern p = Pattern.compile(pattern); 
		while(nutrientIterator.hasNext()) {
			Nutrient n = nutrientIterator.next();
			Matcher m = p.matcher(n.name());
			if (m.find()) {
				rval.add(n);
			}
		}
		return rval;
	}

	/**
	 * Finds the sum of a single nutrient string in the case of > 1 returned nutrients
	 *
	 * @param  nutrientStr a regex matching the nutrient(s)
	 * @return Sum of the amounts for each nutrient 
	 */
	public double nutrientSum(String nutrientStr) {
		ArrayList<Nutrient> nutrients = findNutrients(nutrientStr);
		if (nutrients.size() == 0) {
			return 0.0;
		}

		if (nutrients.size() == 1) {
			return nutrients.get(0).amount(); 
		} 	

		                                                    // more than 1 nutrient with regex found,
		double sum = 0.0;                                   // so add them all. it's likely that only
		for (int x = 0; x < nutrients.size(); x++) {        // one has a value, and the rest will be 0
			sum += nutrients.get(x).amount();                // but catches case where regex appears > 1
		}
		return sum;
	}


	/* Getters */

	public String source() {
		return source;
	}

	public String ndb() {
		return ndb;
	}

	public String upc() {
		return upc;
	}

	public String name() {
		return name;
	}

	public String group() {
		return group;
	}

	public String cn() {
		return common_name;
	}

	public List<Nutrient> nutrients() {
		return nutrients;
	}

	public String ingredients() {
		return ingredients;
	}

	public Date collection_date() {
		return collection_date;
	}

	public String fileName() {
		return fn;
	}
}
