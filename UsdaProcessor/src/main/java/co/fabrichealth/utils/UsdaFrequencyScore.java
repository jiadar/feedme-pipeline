package co.fabrichealth.utils;

import co.fabrichealth.utils.Ingredient;

/** Not fully implemented - Do not use */
public class UsdaFrequencyScore {
 
	private double score;
	
	public void setRatio(Ingredient i) {

		String item_name = i.name();
		int total_chars = item_name.length();
		
		int commas = get_commas(item_name);
		int caps = get_caps(item_name);
		int alphas = total_chars - commas - caps;

		// Compute the interesting ratios

		double ratio1 = 0.45 * commas / (caps + alphas); 
		double ratio2 = 0.10 * alphas / caps;
		double ratio3 = 0.45 * total_chars / 100;

		// Set the score which will be used in the ranking algorithim 
		
		score = ratio1 + ratio2 + ratio3;
	}

	private int get_commas(String s) {
		return s.replaceAll("[^,]", "").length();
			
	}

	private int get_caps(String s) {
		String outputString = "";

		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			s += Character.isUpperCase(c) ? c + " " : ""; 
		}
		
		return outputString.length();		
	}
}
