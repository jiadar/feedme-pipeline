package co.fabrichealth.utils.generators;

/**
 * A generator interface to output nutrition data by implementing generators. 
 *
 * This is called by the UsdaProcessor when attempting to generate a file of this type.  
 *
 * To make a new generator, implement this class. Optionally, extend the CsvGenerator class
 * as it is fairly generic. 
 */
public interface UsdaGenerator {

	/** 
	 * Generates suitable output to the file string returned. 
	 *
	 * @return filename The name of the file containing the generated data
	 */
	public String run(); 

	/** 
	 * Generates suitable output to the file string returned up to max entries. 
	 *
	 * @param max The maximum number of files to process
	 * @return filename The name of the file containing the generated data
	 */
	public String run(int max); 

	/** 
	 * Determines if the generator has run (at least once) or not.
	 *
	 * @return boolean true if the generator has run, false if not
	 */ 
	public boolean hasRun();
	
	/** 
	 * Returns the number of successes detected in parsing. 
	 * If generator has not run, will be 0. 
	 *
	 * @return numSuccesses the number of successful entries generated
	 */
	public int getSuccesses();

	/** 
	 * Returns the number of failures detected in parsing 
	 * If generator has not run, will be 0. 
	 *
	 * @return numFailures the number of unsuccessful entries discarded
	 */
	public int getFailures();

	/** 
	 * Returns the total number of entries attempted to generate
	 * If generator has not run, will be 0. 
	 *
	 * @return total number of total entries attempted to generate
	 */
	public int getTotal();

	/** 
	 * Returns the generated yield as a double less than 1
	 * If generator has not run, will be 0. 
	 *
	 * @return yield successes divided by total
	 */
	public double yield();
}
