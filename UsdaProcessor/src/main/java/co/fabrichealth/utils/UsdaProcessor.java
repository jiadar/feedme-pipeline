package co.fabrichealth.utils;

import java.util.List;
import java.util.logging.Logger;
import java.text.DecimalFormat;
import java.util.ArrayList;
import co.fabrichealth.utils.generators.UsdaGenerator;
import co.fabrichealth.utils.generators.CsvGenerator;
import co.fabrichealth.utils.generators.PostgresGenerator;

/** 
 * Methods to process and report stats on a USDA Files directory.
 *
 */
public class UsdaProcessor {

	private final static Logger LOGGER = Logger.getLogger(UsdaProcessor.class.getName());
	
	String usdaDirectory;
	
	public UsdaProcessor(String directory) {
		this.usdaDirectory = directory;
	}

	// TODO: Fix this hardcode directory reference to something sane
	public UsdaProcessor() {
		this("/Users/ross/work/fabric-data/USDA_FULL/");
	}

	/**
	* Attempt to process all files and return stats
	*
	* It will do this by attempting to add a new ingredient by the ingredient parser.
	* The ingredient parser must first recognize the type, apply the parser, and then return successfully. 
	* If it returns successfully, we check if the name is populated, and check a nutrient. If those check
	* assume success. If those fail, and we detected the type, then we have an error that should be fixed
	* in the logic of the parser. 
	* 
	* In the event a file type can not be detected and matched to an appropriate parser, we fail immediately. 
	*
	* @param generator The generator to use (either postgres or csv) 
	* @param sampleSize the amount of files to process
	* @return csvString a string of stats success, failure, total, % success, % failure
	*/
	public String processWithStats(String generator, int sampleSize) {
		String rval = "";
		UsdaGenerator g;

		switch (generator) {
		case "postgres": 
			g = new PostgresGenerator(usdaDirectory);        // Specificy alternate generator here
			break;
		case "csv":
			g = new CsvGenerator(usdaDirectory);        // Specificy alternate generator here
			break;
		default:
			return "0,0,0,0%,0%";
		}

		g.run(sampleSize);
		
		// extract stats
		rval += g.getSuccesses() + "," + g.getFailures() + "," + g.getTotal() + ",";
		DecimalFormat df = new DecimalFormat("##.###");
		rval += df.format(g.getSuccesses()*100.0/g.getTotal()) + "%" + ",";
		df = new DecimalFormat("##.###");
		rval += df.format(g.getFailures()*100.0/g.getTotal()) + "%";
		return rval;
	}

	/**
	* Attempt to process all files and return stats
	*
	* @param generator The generator to use (either postgres or csv) 
	* @return csvString a string of stats success, failure, total, % success, % failure
	*/
	public String processWithStats(String generator) {
		return processWithStats(generator, 2147483647);
	}
}
