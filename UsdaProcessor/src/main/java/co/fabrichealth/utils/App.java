package co.fabrichealth.utils;

/**
 * The App's main method will attempt to parse all the files in the passed in directory.
 *
 * Following the tests, a count is calculated and a success ratio computed and displayed.  
 * 
 */
public class App {
	public static void main( String[] args ) {

		// Set the number of entries to process from first argument
		final int numToProcess = (Integer.parseInt(args[0]) > 0) ? Integer.parseInt(args[0]) : 2147483647;

		// Set the directory we are reading the files from if it was not passed in, set to a default
		final String USDA_DIR = (args[1].length() > 0) ? args[1] : "/Users/ross/work/fabric-data/USDA_FULL/";

		UsdaProcessor usdaProcessor = new UsdaProcessor(USDA_DIR);

		// TODO: Make the generator selection an enum or other improvement on this
		String stats = usdaProcessor.processWithStats("postgres", numToProcess);
		String[] stat = stats.split(",");

		System.out.println("TOTAL    : " + stat[2]);
		System.out.println("SUCCESS  : " + stat[0]);
		System.out.println("FAILURE  : " + stat[1]);
		System.out.println("% SUCCESS: " + stat[3]);
		System.out.println("% FAILURE: " + stat[4]);
	}
}
