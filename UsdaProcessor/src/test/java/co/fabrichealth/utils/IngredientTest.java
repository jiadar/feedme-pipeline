package co.fabrichealth.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;

import co.fabrichealth.utils.Ingredient;

/* Test Ingredient Class */
public class IngredientTest  {

	// Create a new ingredient and make sure it is empty
	@Test
	public void CreateNewIngredient() {
		Ingredient i = new Ingredient();
		assertEquals(i.ndb(), "");
	}

	// See if an ingredient file can be opened and read. 
	@Test
	public void IngredientFileReads() throws IOException {
		URL url = this.getClass().getResource("/126700");

		try {
			File f = new File(url.getFile());
			BufferedReader b = new BufferedReader(new FileReader(f));
			String line = "";

			line = b.readLine();
			assertNotEquals(line, null);
			b.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}

	// See if the header can be extracted by using a typical file and comparing the headers extracted
	@Test
	public void IngredientShouldExtractHeaders() throws IOException {
		URL url = this.getClass().getResource("/126700");
		File f = new File(url.getFile());
		Ingredient i = new Ingredient(f);

		assertEquals(i.source(),"USDA Branded Food Products Database Release April 2017 Software v.3.7.1 2017-03-29");
		assertEquals(i.name(), "SUPER FRESH! FOODS BBQ CHOPPED SALAD WITH SMOKED TOFU & RANCH DRESSING");
		assertEquals(i.group(), "Branded Food Products Database");
	   assertEquals(i.cn(), "");
		assertEquals(i.ndb(), "45168497");
		assertEquals(i.upc(), "614294001447");
	}

	// See if nutrients can be extracted by checking for the existance of or absense of nutrients and
	// the value of a particular nutrient
	@Test
	public void IngredientShouldExtractNutrients() throws IOException {
		URL url = this.getClass().getResource("/126700");
		File f = new File(url.getFile());
		Ingredient i = new Ingredient(f);

		assertEquals(i.findNutrient("Carb").isPresent(), true);
		assertEquals(i.findNutrient("Cake").isPresent(), false);
		assertEquals(i.findNutrient("Carb").get().amount(), 9.86, 0.01);
		assertEquals(i.findNutrient("Calcium, Ca").get().amount(), 70.0, 0.001);

	}

	// See if ingredients can be extracted from the particular type
	@Test
	public void IngredientShouldExtractIngredients() throws IOException {
		URL url = this.getClass().getResource("/126700");
		File f = new File(url.getFile());
		Ingredient i = new Ingredient(f);

		assertEquals(i.ingredients(), "ROMAINE LETTUCE, SMOKED TOFU (ORGANIC SOYBEANS, WATER, MAGNESIUM CHLORIDE, SUCANAT, ORGANIC SUGAR, MOLASSES, BLACK PEPPER, SALT, CAYENNE, HICKORY SMOKE), ORGANIC BLACK BEANS (ORGANIC BLACK BEANS, WATER, SEA SALT), CHERRY TOMATO, MONTEREY JACK CHEESE (CULT");
	}

	// See if the standard reference type parses
	@Test
	public void IngredientNutrientDataStdReferenceParses() throws IOException {
		URL url = this.getClass().getResource("/1347");
		File f = new File(url.getFile());
		Ingredient i = new Ingredient(f);

		assertEquals(i.ndb(), "06233");
		assertEquals(i.name(), "PREGO Pasta, Organic Tomato and Basil Italian Sauce, ready-to-serve");
		assertEquals(i.group(), "Soups, Sauces, and Gravies");
		assertEquals(i.findNutrient("Carb").isPresent(), true);
		assertEquals(i.findNutrient("Cake").isPresent(), false);
		assertEquals(i.findNutrient("Carb").get().amount(), 10.4, 0.01);
		assertEquals(i.findNutrient("Calcium, Ca").get().amount(), 32.0, 0.001);
	}
 
	// Check a different variation of the standard reference type that has sources with a header
	@Test
	public void IngredientNutrientDataStdReferenceParsesSourcesWithId() throws IOException {
		URL url = this.getClass().getResource("/1");
		File f = new File(url.getFile());
		Ingredient i = new Ingredient(f);

		assertEquals(i.ndb(), "01001");
		assertEquals(i.name(), "Butter, salted");
		assertEquals(i.group(), "Dairy and Egg Products");
		assertEquals(i.findNutrient("Betaine").isPresent(), true);
		assertEquals(i.findNutrient("CakeIsDelicious").isPresent(), false);
		assertEquals(i.findNutrient("Betaine").get().amount(), 0.3, 0.001);
		assertEquals(i.findNutrient("Calcium, Ca").get().amount(), 24.0, 0.001);
	}

	// Check a different variation of the standard reference type that has sources without a header
	@Test
	public void IngredientNutrientDataStdReferenceParsesSourcesWithoutId() throws IOException {
		URL url = this.getClass().getResource("/6507");
		File f = new File(url.getFile());
		Ingredient i = new Ingredient(f);

		assertEquals(i.ndb(), "20038");
		assertEquals(i.name(), "Oats");
		assertEquals(i.group(), "Cereal Grains and Pasta");
		assertEquals(i.findNutrient("Protein").isPresent(), true);
		assertEquals(i.findNutrient("CakeIsDelicious").isPresent(), false);
		assertEquals(i.findNutrient("Protein").get().amount(), 16.89, 0.001);
		assertEquals(i.findNutrient("Calcium, Ca").get().amount(), 54.0, 0.001);		
	}

	@Test
	public void IngredientShouldParseGtin() throws IOException {
		URL url = this.getClass().getResource("/94334");
		File f = new File(url.getFile());
		Ingredient i = new Ingredient(f);

		assertEquals(i.ndb(), "45135530");
		assertEquals(i.name(), "CAMPBELL'S SOUP PASTA UNPREPARED");
		assertEquals(i.group(), "Branded Food Products Database");
		assertEquals(i.upc(), "00051000186485");
		assertEquals(i.findNutrient("Protein").isPresent(), true);
		assertEquals(i.findNutrient("CakeIsDelicious").isPresent(), false);
		assertEquals(i.findNutrient("Protein").get().amount(), 2.38, 0.001);
		assertEquals(i.findNutrient("Calcium, Ca").get().amount(), 12.0, 0.001);
	}

	@Test
	public void IngredientShouldParseFatCorrectly() throws IOException {
		URL url = this.getClass().getResource("/6372");
		File f = new File(url.getFile());
		Ingredient i = new Ingredient(f);
		assertEquals(i.findNutrient("Selenium").isPresent(), true);
		assertEquals(i.findNutrient("Selenium").get().amount(), 8.1, 0.001);
		assertEquals(i.findNutrient("Total lipid").isPresent(), true);
		assertEquals(i.findNutrient("Total lipid").get().amount(), 20.80, 0.001);
		assertEquals(i.findNutrient("Calcium, Ca").get().amount(), 21.0, 0.001);
	}

	@Test
	public void IngredientShouldParseCalciumCorrectly1() throws IOException {
		URL url = this.getClass().getResource("/122292");
		File f = new File(url.getFile());
		Ingredient i = new Ingredient(f);
		assertEquals(i.findNutrient("Sodium").isPresent(), true);
		assertEquals(i.findNutrient("Sodium").get().amount(), 306.0, 0.001);
		assertEquals(i.findNutrient("Total lipid").isPresent(), true);
		assertEquals(i.findNutrient("Total lipid").get().amount(), 38.89, 0.001);
		assertEquals(i.findNutrient("Calcium, Ca").get().amount(), 56.0, 0.001);
	}

	@Test
	public void IngredientShouldParseCalciumCorrectly2() throws IOException {
		URL url = this.getClass().getResource("/7824");
		File f = new File(url.getFile());
		Ingredient i = new Ingredient(f);
		assertEquals(i.findNutrient("Calcium, Ca").get().amount(), 232.0, 0.001);
	}
}

