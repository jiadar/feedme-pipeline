-- Note that as of Sep 6 2017 isearch table has been removed and queries relying on it will no longer function

/* Following from design doc
 * Determine the different categories 
*/

select count(*) from ingredients where lower(name) like lower('%Tomato%')
and is_category>0 and is_liquid_type=0;

select distinct is_category,
(select isearch.descr from isearch where is_category=isearch.id) as category_name 
from ingredients where lower(name) like lower('%Tomato%')
and is_category>0 and is_liquid_type=0;

select count(is_category) as count_BabyFoods from ingredients where lower(name) like lower('%Tomato%')
and is_category>0 and is_liquid_type=0 and is_category=2;

select count(is_category) as count_FastFoods from ingredients where lower(name) like lower('%Tomato%')
and is_category>0 and is_liquid_type=0 and is_category=10;

select count(is_category) as count_FatsAndOils from ingredients where lower(name) like lower('%Tomato%')
and is_category>0 and is_liquid_type=0 and is_category=11;

select count(is_category) as count_Legumes from ingredients where lower(name) like lower('%Tomato%')
and is_category>0 and is_liquid_type=0 and is_category=15;

select count(is_category) as count_Meals from ingredients where lower(name) like lower('%Tomato%')
and is_category>0 and is_liquid_type=0 and is_category=16;

select count(is_category) as count_Soups from ingredients where lower(name) like lower('%Tomato%')
and is_category>0 and is_liquid_type=0 and is_category=23;

select count(is_category) as count_Vegetables from ingredients where lower(name) like lower('%Tomato%')
and is_category>0 and is_liquid_type=0 and is_category=26;








