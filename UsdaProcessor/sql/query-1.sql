-- Note that as of Sep 6 2017 isearch table has been removed and queries relying on it will no longer function

/*
This queries the count of each filter field in ingredients
To run this file and have pretty output, try the following: 

psql < query-1.sql | grep -v \( | grep -v - | sed -e 's/ //g' | 
awk '/[a-z_]+$/ { printf("%s: ", $0); next } 1'  | awk '/^$/ {next} 1'

*/

select count(is_category) as count_category from ingredients where lower(name) like lower('%Tomato%') and is_category>0;    
select count(is_prep) as count_prep from ingredients where lower(name) like lower('%Tomato%') and is_prep>0;        
select count(is_upc) as count_upc from ingredients where lower(name) like lower('%Tomato%') and is_upc>0;         
select count(is_brand) as count_brand from ingredients where lower(name) like lower('%Tomato%') and is_brand>0;       
select count(is_branded) as count_branded from ingredients where lower(name) like lower('%Tomato%') and is_branded>0;     
select count(is_organic) as count_organic from ingredients where lower(name) like lower('%Tomato%') and is_organic>0;     
select count(is_cook_method) as count_cook_method from ingredients where lower(name) like lower('%Tomato%') and is_cook_method>0; 
select count(is_storage) as count_storage from ingredients where lower(name) like lower('%Tomato%') and is_storage>0;     
select count(is_modified) as count_modified from ingredients where lower(name) like lower('%Tomato%') and is_modified>0;    
select count(is_matter_type) as count_matter_type from ingredients where lower(name) like lower('%Tomato%') and is_matter_type>0; 
select count(is_liquid_type) as count_liquid_type from ingredients where lower(name) like lower('%Tomato%') and is_liquid_type>0; 
select count(is_pyramid) as count_pyramid from ingredients where lower(name) like lower('%Tomato%') and is_pyramid>0;     
select count(is_color) as count_color from ingredients where lower(name) like lower('%Tomato%') and is_color>0;       
select count(is_size) as count_size from ingredients where lower(name) like lower('%Tomato%') and is_size>0;        

/*
select count(*) as category_distinct from (select distinct is_category as count_category from ingredients where lower(name) like lower('%Tomato%') and is_category>0) as x;    
select count(*) as prep_distinct from (select distinct is_prep as count_prep from ingredients where lower(name) like lower('%Tomato%') and is_prep>0) as x;        
select count(*) as upc_distinct from (select distinct is_upc as count_upc from ingredients where lower(name) like lower('%Tomato%') and is_upc>0) as x;         
select count(*) as brand_distinct from (select distinct is_brand as count_brand from ingredients where lower(name) like lower('%Tomato%') and is_brand>0) as x;       
select count(*) as branded_distinct from (select distinct is_branded as count_branded from ingredients where lower(name) like lower('%Tomato%') and is_branded>0) as x;     
select count(*) as organic_distinct from (select distinct is_organic as count_organic from ingredients where lower(name) like lower('%Tomato%') and is_organic>0) as x;     
select count(*) as cook_method_distinct from (select distinct is_cook_method as count_cook_method from ingredients where lower(name) like lower('%Tomato%') and is_cook_method>0) as x; 
select count(*) as storage_distinct from (select distinct is_storage as count_storage from ingredients where lower(name) like lower('%Tomato%') and is_storage>0) as x;     
select count(*) as modified_distinct from (select distinct is_modified as count_modified from ingredients where lower(name) like lower('%Tomato%') and is_modified>0) as x;    
select count(*) as matter_type_distinct from (select distinct is_matter_type as count_matter_type from ingredients where lower(name) like lower('%Tomato%') and is_matter_type>0) as x; 
select count(*) as liquid_type_distinct from (select distinct is_liquid_type as count_liquid_type from ingredients where lower(name) like lower('%Tomato%') and is_liquid_type>0) as x; 
select count(*) as pyramid_distinct from (select distinct is_pyramid as count_pyramid from ingredients where lower(name) like lower('%Tomato%') and is_pyramid>0) as x;     
select count(*) as color_distinct from (select distinct is_color as count_color from ingredients where lower(name) like lower('%Tomato%') and is_color>0) as x;       
select count(*) as size_distinct from (select distinct is_size as count_size from ingredients where lower(name) like lower('%Tomato%') and is_size>0) as x;        
*/



