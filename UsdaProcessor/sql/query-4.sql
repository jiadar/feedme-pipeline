-- Note that as of Sep 6 2017 isearch table has been removed and queries relying on it will no longer function

/* Following from design doc, set is_liquid_type = 0 
 * Determine the count for different color types
*/

select count(*) from ingredients where lower(name) like lower('%Tomato%')
and is_category>0 and is_liquid_type=0 and is_category=26;

-- Select is_liquid_type=0 and is_category=26

select count(is_category) as count_category from ingredients where lower(name) like lower('%Tomato%') and is_category>0 and is_liquid_type=0 and is_branded=0 and is_category=26;    
select count(is_prep) as count_prep from ingredients where lower(name) like lower('%Tomato%') and is_prep>0 and is_liquid_type=0 and is_branded=0 and is_category=26;        
select count(is_upc) as count_upc from ingredients where lower(name) like lower('%Tomato%') and is_upc>0 and is_liquid_type=0 and is_branded=0 and is_category=26;         
select count(is_brand) as count_brand from ingredients where lower(name) like lower('%Tomato%') and is_brand>0 and is_liquid_type=0 and is_branded=0 and is_category=26;       
select count(is_branded) as count_branded from ingredients where lower(name) like lower('%Tomato%') and is_branded>0 and is_liquid_type=0 and is_branded=0 and is_category=26;     
select count(is_organic) as count_organic from ingredients where lower(name) like lower('%Tomato%') and is_organic>0 and is_liquid_type=0 and is_branded=0 and is_category=26;     
select count(is_cook_method) as count_cook_method from ingredients where lower(name) like lower('%Tomato%') and is_cook_method>0 and is_liquid_type=0 and is_branded=0 and is_category=26; 
select count(is_storage) as count_storage from ingredients where lower(name) like lower('%Tomato%') and is_storage>0 and is_liquid_type=0 and is_branded=0 and is_category=26;     
select count(is_modified) as count_modified from ingredients where lower(name) like lower('%Tomato%') and is_modified>0 and is_liquid_type=0 and is_branded=0 and is_category=26;    
select count(is_matter_type) as count_matter_type from ingredients where lower(name) like lower('%Tomato%') and is_matter_type>0 and is_liquid_type=0 and is_branded=0 and is_category=26; 
select count(is_liquid_type) as count_liquid_type from ingredients where lower(name) like lower('%Tomato%') and is_liquid_type>0 and is_liquid_type=0 and is_branded=0 and is_category=26; 
select count(is_pyramid) as count_pyramid from ingredients where lower(name) like lower('%Tomato%') and is_pyramid>0 and is_liquid_type=0 and is_branded=0 and is_category=26;     
select count(is_color) as count_color from ingredients where lower(name) like lower('%Tomato%') and is_color>0 and is_liquid_type=0 and is_branded=0 and is_category=26;       
select count(is_size) as count_size from ingredients where lower(name) like lower('%Tomato%') and is_size>0 and is_liquid_type=0 and is_branded=0 and is_category=26;


select distinct is_color,
(select isearch.descr from isearch where is_color=isearch.id) as color_name 
from ingredients where lower(name) like lower('%Tomato%')
and is_category=26 and is_liquid_type=0 and is_category=26;

select count(is_color) as count_Black from ingredients where lower(name) like lower('%Tomato%')
and is_category=26 and is_liquid_type=0 and is_color=74;

select count(is_color) as count_Red from ingredients where lower(name) like lower('%Tomato%')
and is_category=26 and is_liquid_type=0 and is_color=76;

select count(is_color) as count_Orange from ingredients where lower(name) like lower('%Tomato%')
and is_category=26 and is_liquid_type=0 and is_color=77;

select count(is_color) as count_Green from ingredients where lower(name) like lower('%Tomato%')
and is_category=26 and is_liquid_type=0 and is_color=80;





